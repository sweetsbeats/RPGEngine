#pragma once
#include <SFML/Graphics.hpp>
#include "GameState.h"




class MenuState : public GameState
{

private:
	sf::RectangleShape rect;

public:
	MenuState() {}
	MenuState(sf::RenderWindow &window);
	~MenuState();


	void init();

	virtual void update(float dt, StateManager &stateManager);

	virtual void render();


};
