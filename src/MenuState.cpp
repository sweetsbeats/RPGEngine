#include "MenuState.h"
#include "StateManager.h"




MenuState::MenuState(sf::RenderWindow &window)
{
	window_ptr = &window;
	MenuState::init();

}




void MenuState::init()
{
	
	rect.setSize(sf::Vector2f(100, 100));
	rect.setFillColor(sf::Color::Red);
	rect.setPosition(sf::Vector2f(100, 100));

}


void MenuState::update(float dt, StateManager &stateManager)
{	
	if (input.keyboard.isKeyPressed(sf::Keyboard::F))
	{	
		stateManager.switchState(stateManager.Playing);
		
	}
}

void MenuState::render()
{
	window_ptr->draw(rect);
}







MenuState::~MenuState()
{
}
