#include "Player.h"
#include <iostream>
#include <ctgmath>



Player::Player()
{
}


Player::~Player()
{
}


void Player::init(sf::RenderWindow &window)
{

//	AnimatedSprite animSprite;
	animSprite.setPosition( sf::Vector2f(200, 200) );
	currentAnimation = &walkDown;

	texture.loadFromFile("textures/Player.png");
	initAnimations();

	window_ptr = &window;

	isWalking = false;

}


void Player::update(float dt)
{
	//get frameTime to appease the animatedSprite's update
	sf::Time frameTime = frameClock.restart();

	//				ADD STUFF HERE

	// reset velocity on update
	sf::Vector2f velocity;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		currentAnimation = &walkUp;
		velocity.y -= speed * dt;
		isWalking = true;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		currentAnimation = &walkDown;
		velocity.y += speed * dt;
		isWalking = true;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		currentAnimation = &walkLeft;
		velocity.x -= speed * dt;
		isWalking = true;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		currentAnimation = &walkRight;
		velocity.x += speed * dt;
		isWalking = true;
	}

	//Normalize vector
	if (velocity.x != 0.0f && velocity.y != 0.0f)
	{ velocity /= std::sqrt(2.0f); }





	//				STOP ADDING STUFF HERE


	animSprite.play( *currentAnimation );

	//apply velocity
	animSprite.move(velocity);

	//if not moving, stop animations
	if (!isWalking)
	{ animSprite.stop(); }

	//Reset walking bool; If player stops moving next update, code above catches it
	isWalking = false;

	animSprite.update(frameTime);
}


void Player::draw()
{

	window_ptr->draw(animSprite);

}


void Player::initAnimations()
{
	//give animations the spritesheet

	//TODO: Come up with cleaner way to hold animations, maybe a vector

	walkUp.setSpriteSheet(texture);
	walkDown.setSpriteSheet(texture);
	walkLeft.setSpriteSheet(texture);
	walkRight.setSpriteSheet(texture);




	//Add walkUp frames
	walkUp.addFrame(sf::IntRect(32, 96, 32, 32));
	walkUp.addFrame(sf::IntRect(64, 96, 32, 32));
	walkUp.addFrame(sf::IntRect(32, 96, 32, 32));
	walkUp.addFrame(sf::IntRect(0, 96, 32, 32));

	//Add walkDown frames
	walkDown.addFrame(sf::IntRect(32, 0, 32, 32));
	walkDown.addFrame(sf::IntRect(64, 0, 32, 32));
	walkDown.addFrame(sf::IntRect(32, 0, 32, 32));
	walkDown.addFrame(sf::IntRect(0, 0, 32, 32));


	//Add walkLeft frames
	walkLeft.addFrame(sf::IntRect(32, 32, 32, 32));
	walkLeft.addFrame(sf::IntRect(64, 32, 32, 32));
	walkLeft.addFrame(sf::IntRect(32, 32, 32, 32));
	walkLeft.addFrame(sf::IntRect(0, 32, 32, 32));

	//Add walkRight frames
	walkRight.addFrame(sf::IntRect(32, 64, 32, 32));
	walkRight.addFrame(sf::IntRect(64, 64, 32, 32));
	walkRight.addFrame(sf::IntRect(32, 64, 32, 32));
	walkRight.addFrame(sf::IntRect(0, 64, 32, 32));

}
