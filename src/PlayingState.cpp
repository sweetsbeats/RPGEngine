#include "PlayingState.h"
#include "GameState.h"


PlayingState::PlayingState(sf::RenderWindow &window)
{
	window_ptr = &window;

	//player.init();

}


void PlayingState::init()
{

	player.init(*window_ptr);
}



void PlayingState::update(float dt, StateManager &stateManager)
{
	player.update(dt);
}



void PlayingState::render()
{
	player.draw();
}















PlayingState::~PlayingState()
{
}
