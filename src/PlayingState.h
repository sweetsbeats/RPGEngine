#pragma once
#include "GameState.h"
#include "Player.h"


class PlayingState : public GameState
{


private:

	Player player;




public:
	PlayingState() {}
	PlayingState(sf::RenderWindow &window);
	~PlayingState();


	void init();

	virtual void update(float dt, StateManager &stateManager);

	virtual void render();

};

