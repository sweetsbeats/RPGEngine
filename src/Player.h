#pragma once

#include "Entity.h"

#include <SFML/Graphics.hpp>

class Player : Entity
{

private:

//	std::vector<Animation>animations;



	Animation walkUp;
	Animation walkDown;
	Animation walkLeft;
	Animation walkRight;



	bool isWalking;

	float speed = 200;

public:
	Player();
	~Player();


	void init(sf::RenderWindow &window);

	//virtual void init( std::string textureName );

	virtual void update(float dt);

	virtual void draw();


	void initAnimations();


};
